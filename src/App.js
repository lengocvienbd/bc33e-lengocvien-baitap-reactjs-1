import logo from "./logo.svg";
import "./App.css";
import List from "./Ex_Layout/resource/List";
import Footer from "./Ex_Layout/resource/Footer";
import Header from "./Ex_Layout/resource/Header";
import Banner from "./Ex_Layout/resource/Banner";

function App() {
  return (
    <div className="App">
      <Header />
      <Banner />
      <List />
      <Footer />
    </div>
  );
}

export default App;
